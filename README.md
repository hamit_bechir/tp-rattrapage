 # TP RATTRAPAGE
 
TP Finale: Forker le repo et faites le fichier CI associé.

Avant de débuter la CI : Créer un channel privé Slack à votre nom, et m'inviter dedans avec la commande`/invite @...`.
Ajouter moi en `owner` du repos, via l'email : contact@thomas-brunet.fr
Envoyer le lien de votre projet sur le channel privée SLACK.
Le projet est un bot pour le tchat mattermost codé en JS.
Le projet dispose de scripts disponible sur le fichier`package.json`. Ils sont exécutable via la commande`npm run $SCRIPT_NAME`.
Toutes les commandes`NPM`sont testables sur votre machine.
La CI devra au maximum utiliser les variables de l'appli Gitlab et éviter d'en mettre dans le fichier`.gitlab-ci.yml`
Le fichier de CI devra être <u>unique</u> entre les différentes branches.
<u>Aucun commit après 13h ne sera pris en compte.</u>
Je suis disponible en tant que "développeur" pour répondre à des questions basiques, sur slack.
Url du repo :`https://gitlab.com/Porthole/tp-rattrapage`

La ci devra effectuer les actions suivantes :
- Push sur`master`/ ou sur un`tag`: Générer l'`apidoc`/ Envoyer l'api doc générée sur le serveur dans un dossier`prod`/ Envoyer un message sur slack sur le channel privé indiquant que la mise en prod est terminée en version`latest`/ ou en version`$TAG`, avec le lien de la nouvelle api doc
- Push sur`develop`:Lancer un`prettier`/ Lancer le`lint`/ Lancer les`tests` / Envoyer le rapport de test (`success`ou`failure`) généré sur le serveur dans un dossier test / En cas de fail des tests, envoyez un message sur slack avec l'URL du rapport de test / Générer l'`apidoc`/ Envoyer l'api doc générée sur le serveur dans un dossier`dev
- Push sur une branche ayant comme nom`feature/.*`: Lancer un`prettier`/ Lancer le`lint`
- Toutes les jours à 18h30 : Envoyer une analyse du code via`sonar-scanner`sur le sonarqube


Ressources à dispositions :
- https://sonarqube.devzone.fr
- https://ynovci2020.slack.com
- Identifiant serveur web ssh :
- username: student
- password: itsasecret
- adresse ssh:` 51.75.126.124`
- port ssh : 30022
- dossier où déposer vos contenu :` /var/www/YOUR_NAME`
- adresse web : `http://51.75.126.124:30080`
- clé privée ssh en snippet en dessous 
